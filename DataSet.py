from math import sqrt
from os import path
import csv

class DataSet():

	#dataSetFileName - name of csv file (make sure that there are no empty lines in files)
	#(if there are multiple empty lines at the end of the file output column will not be transformed into multiple columns)

	#numberOfColumns - columns in csv file (ie. input coulmns + expected output columns)
	#ex. in case of iris data set there are 5 columns (4 input ones and one for expected output)
	
	#number of output columns in the csv file(ie. value numberOfColumns passed to constructor of DataSet) 
	#may not correspond to number of output columns created (check latter via getNumberOfOutputs())
	def getNumberOfOutputs(self): return self.numberOfOutputs

	#indexOfLastInputColumn - first column is 1
	#ex. in case of iris data set which has 5 columns and first 4 are input parameters indexOfLastInputColumn should equal 4
	def __init__(self, dataSetFileName, numberOfColumns, indexOfLastInputColumn):

		assert indexOfLastInputColumn < numberOfColumns, 'indexOfLastInputColumn must come before last column(ie. numberOfColumns)'

		#key - string to be replaced by
		#value - float value replacing it in range from 0.0 to 1.0
		self.stringsTokensMap = {}
		self.floats = []#holds as many floats in 0.0 to 1.0 range as there are strings in one row of csv file

		self.inputs = []
		self.outputs = []

		with open(path.join(path.dirname(__file__), 'data', dataSetFileName)) as csv_file:

			try:
				dialect = csv.Sniffer().sniff(csv_file.read(1024))
				#Perform various checks on the dialect (e.g., lineseparator, delimiter) to make sure it's sane
				csv_file.seek(0)#reset the read position back to the start of the file before reading any entries
			except csv.Error:
				#File appears not to be in CSV format
				print('file ', dataSetFileName, 'is not valid csv format')
				raise SystemExit('only csv files can be interpreted by DataSet')
			
			csv_reader = csv.reader(csv_file, delimiter=',')


			#count the number of strings that are in first row (that number should be same in all other rows)
			stringsInRow = 0

			uniqueStrings = set()
			for row in csv_reader:
				for elem in row:
					try:
						elem = float(elem)
					except ValueError: pass#in this case value remains str and will be replaced with token
					if type(elem) == str:
						uniqueStrings.add(elem)

			stringsInRow = len(uniqueStrings)
			
			csv_file.seek(0)#make sure that first row is accessable in next csv_reader itteration


			if stringsInRow != 0:
				#generate stringsInRow number of floats and save them in self.floats
				#(ex. if there are 4 stringsInRow floats are 0.25, 0.5, 0.75 and 1.0)
				#(ex. if there are 3 stringsInRow floats are 0.33, 0.66 and 1.0)
				step = 1.0/stringsInRow
				self.floats = [i*step for i in range(1, stringsInRow+1)]


			for row in csv_reader:

				rowInputValues = []
				rowOutputValues = []

				for i in range(numberOfColumns):


					if type(row) != list or len(row) != numberOfColumns:
						print('Warrning failed to load past row', row, 'whose length is', len(row), 'because it differes from first row')
						self.numberOfOutputs = numberOfColumns - indexOfLastInputColumn
						return

					#because row[i] type will always be str try to convert it to float
					value = row[i]
					try:
						value = float(row[i])
						#value /= 10.#normalize iris dataset by making sure its values are in 0.0-1.0 range
						#assert 0.0 <= value and value <= 1.0, 'value:'+str(value)+' is not within range 0.0 and 1.0'
					except ValueError: pass#in this case value remains str and will be replaced with token

					if i <= indexOfLastInputColumn-1:#-1 because coulumns are starting from 0
						if type(value) == str: rowInputValues.append(self.getToken(value))
						else:                  rowInputValues.append(value)
					else:
						if type(value) == str: rowOutputValues.append(self.getToken(value))
						else:                  rowOutputValues.append(value)

				self.inputs.append(rowInputValues)
				self.outputs.append(rowOutputValues)


			#if there is only one output column in the data, but it can hold more than 2 distinct values
			#(ex. how Iris data set has 3 possible distinct values)
			#it is better to have in a neural network as many output layer neurons as there are distinct values
			#(rather then only one output neuron and rely on nn learing that 0.33, 0.66 and 1.0 are distinct values in that one neuron)
			newOutputs = []

			if len(self.outputs[0]) == 1:
				if self.getNumberOfDistinctValues(self.outputs) > 2:

					#ex.
					#self.outputs = [      [0.3],           [0.6],           [0.3],           [1.0]      ]
					#transform into neuron activation of 3 neurons
					#self.outputs = [ [1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [1.0, 0.0, 0.0], [0.0, 0.0, 1.0] ]


					#1. get all possible combinations for newOutputs elements
					combinations = []
					for i in range(self.getNumberOfDistinctValues(self.outputs)):

						#[0, 0, 0] in case of 3 distinct values
						newElem = [0.0 for x in range(self.getNumberOfDistinctValues(self.outputs))]

						#if i==0 [1, 0, 0]
						#if i==1 [0, 1, 0]
						#if i==2 [0, 0, 1]
						newElem[i] = 1.0

						combinations.append(newElem)


					self.mapping = {}
					#2. get distinct values from self.outputs
					#3. map from 1. to 2. (every distinct output value to distinct 0,1 list)
					for elem, newElem in zip(self.getDistinctValues(self.outputs), combinations):
						self.mapping[elem] = newElem


					for output in self.outputs:

						#4. use mapping from 3. to replace single values by 1,0 lists
						newOutputs.append(self.mapping[output[0]])# place value for key output


					self.numberOfOutputs = self.getNumberOfDistinctValues(self.outputs)
					self.outputs = newOutputs


				else: self.numberOfOutputs = numberOfColumns - indexOfLastInputColumn
			else: self.numberOfOutputs = numberOfColumns - indexOfLastInputColumn

		self.printMapping()


	#this function assumes that every sublist has only one element
	#ex. listOfLists = [[1], [2], [1]]                     => 2
	#ex. listOfLists = [[1], [2], [3], [1], [2]]           => 3
	#ex. listOfLists = [[0.3], [0.6], [1.0], [0.3], [1.0]] => 3
	def getNumberOfDistinctValues(self, listOfLists):
		return len(set(tuple(l) for l in listOfLists))

	#this function assumes that every sublist has only one element
	def getDistinctValues(self, listOfLists):
		#print('getDistinctValues:', set(str(l) for l in listOfLists))
		return list(set(l[0] for l in listOfLists))


	#returns token which will stand for that string
	#if function is called multiple times with same stringValue each time returned token will be the same
	def getToken(self, stringValue):

		if stringValue in self.stringsTokensMap.keys():
			#return value for key stringValue which is in stringsTokensMap
			return self.stringsTokensMap[stringValue]

		else:#first time getToken is called with this stringValue

			assert len(self.floats) > 0, 'Error: number of floats generated to replace strings is inefficient'

			#create new key,value pair in self.stringsTokensMap
			self.stringsTokensMap[stringValue] = self.floats.pop()

			#return value for key stringValue which was just added to stringsTokensMap
			return self.stringsTokensMap[stringValue]

	def printMapping(self):
		print()
		try:
			for k, v in self.stringsTokensMap.items():
				print(k, '=>', self.mapping[v])
		except AttributeError: pass#no mapping was created
		print()

	def getMapping(self):
		mapped = {}
		try:
			for k, v in self.stringsTokensMap.items():
				mapped[k] = self.mapping[v]
		except AttributeError: pass#no mapping was created
		return mapped