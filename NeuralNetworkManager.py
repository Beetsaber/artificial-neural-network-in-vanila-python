from Neuron import Neuron
from NeuralNetwork import NeuralNetwork
from NeuralNetworkData import NeuralNetworkData

from random import uniform
from os import system, path
from math import floor, ceil

def normalRound(n):
	if n - floor(n) < 0.5:
		return floor(n)
	return ceil(n)


class NeuralNetworkManager():

	#nnSize is a list containing how many neurons are in every layer
	#nnSize = [neuronsInInputLayer, neuronsInLayer1, neuronsInLayer2,..., neuronsInOutputLayer])

	#inputValues and targetValues are lists containing lists of values for input and desired output
	#ex. inputValues = [[i1,i2], [i3, i4]]
	#ex. targetValues = [[o12], [o34]]

	#weights is a list of weights in initial state; leaving weights==[] means random weights will be used
	
	#ex. XOR call with 2 hidden layers of 3 neurons each:
	#NeuralNetworkManager([2, 3, 3, 1], [[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0]], [[0.0], [1.0], [1.0], [0.0]], False)
	def __init__(self, nnSize, inputValues, targetValues, separateTestData = True, maxEpochsToRun = 30000, verbose = False, weights = []):
		self.nnSize = nnSize
		self.verbose = verbose
		nnData = NeuralNetworkData(inputValues, targetValues, separateTestData)
		
		self.nn = NeuralNetwork(len(nnSize))
		
		#build up neurons for every layer
		for layer, numberOfNeurons in enumerate(nnSize):
			
			neurons = []
				
			#neurons in input layer don't have any weights
			if layer==0:
				for i in range(numberOfNeurons):
					neurons.append(Neuron(i, None))
			
			#neurons in all other layers have as many weights as there are neurons in previous layer
			else:
				for neuronNum in range(numberOfNeurons):
					
					#if random weights
					if weights == []:
						neurons.append(Neuron( neuronNum, [uniform(0.1, 1.) for _ in range(nnSize[layer-1])] ))
						continue#skip custom weights part
					
					#if custom weights
					wTemp = []
					
					for w in weights[layer-1][neuronNum]:
						wTemp.append(w)
								  
					neurons.append(Neuron( neuronNum, wTemp ))
					

			self.nn.addNeuronsToLayer(layer+1, neurons)
		

		try:
			if maxEpochsToRun > 0: print('Starting epoch 1 of', maxEpochsToRun)
			i = 0
			while i < maxEpochsToRun:
				i += 1

				if i==1 or i==maxEpochsToRun or i%100==0:
					system('cls')#this makes program when run in console more readable, yet makes it incorrect in some IDE-s
					print("EPOCH", i)
					if self.epoch(self.nn, nnData, True, separateTestData): break
				else:
					if self.epoch(self.nn, nnData, False, separateTestData): break
			
		except KeyboardInterrupt: print('Ctrl+C')


	def saveNN(self, fileName = 'model.txt'):
		if input('Are you sure you want to overwrite ' + fileName + '? (y/n):').lower() == 'y':
			fullPath = path.join(path.dirname(__file__), 'data', fileName)
			self.nn.save(fullPath)

	def loadNN(self, inputs, outputs, fileName = 'model.txt'):
		fullPath = path.join(path.dirname(__file__), 'data', fileName)
		self.nn.load(fullPath)

		errorCombined = 0
		highestError = 0
		for inputValues, desiredOutputValues in zip(inputs, outputs):

			_ = self.nn.propagate(inputValues)
			error = self.nn.errorRate(self.nn.getOutputs(), desiredOutputValues)
			errorCombined += error

			if error > highestError:
				highestError = error

		print('Average error on training data =', errorCombined/float(len(inputs)))
		print('Error on training example with highest error =', highestError)


	#roundOutputsIntoInts==True will round the highest value to 1 (even when highest value is 0.001) and all others to 0
	def guess(self, inputValues, roundOutputsIntoInts = False):

		if inputValues == []: raise SystemExit('Error [] supplied as inputValues to NeuralNetworkManager')

		self.nn.propagate(inputValues)
		outputs = self.nn.getOutputs()


		if roundOutputsIntoInts == False: return outputs


		nnOutput = []
		#for neuronOutput in outputs:
		#    nnOutput.append(normalRound(neuronOutput))#problem is that there can be multiple values >0.5
		
		indexOfMaxValue = outputs.index(max(outputs))

		if normalRound(outputs[indexOfMaxValue]) == 1: pass#highest value is indeed 1
		else: pass#highest value can only be rounded to 0 (because it is <0.5) so maybe set indexOfMaxValue to -1(all 0 output)

		for i, val in enumerate(outputs):
			if i == indexOfMaxValue:
				nnOutput.append( 1.0 )
			else:
				nnOutput.append( 0.0 )

		return nnOutput


	#returns True if error is tiny(< goalError)
	def epoch(self, nn, nnData, displayTest, separateTestData):

		errorBefore = 0.
		errorAfter = 0.
		errorAfterCombined = 0
		TSSEG = 0.45#training subset error goal
		TSSEGAchieved = True
		highestError = 0

		for inputValues, desiredOutputValues in nnData.getAllTrainingData():

			if self.verbose: print('input =', inputValues, ', target =', desiredOutputValues, end='   ')

			#not only get the output layer values, but also set new output values for neurons
			#so that gradients can be latter calculated from those values
			nnOutput = nn.propagate(inputValues)
			if self.verbose: print('output:', nnOutput)


			if self.verbose: errorBefore = nn.errorRate(nn.getOutputs(), desiredOutputValues)
			if self.verbose: print('Error before backprop:', format(errorBefore, '.4f'), end='  ')

			nn.backprop(desiredOutputValues)#calculate new weights

			if self.verbose: _ = nn.propagate(inputValues)#use new weights to get output values in order to do error calculation
			errorAfter = nn.errorRate(nn.getOutputs(), desiredOutputValues)
			if self.verbose: print('Error after backprop:', format(errorAfter, '.4f'), end='')
			
			if errorAfter > TSSEG: TSSEGAchieved = False
			errorAfterCombined += errorAfter

			if errorAfter > highestError:
				highestError = errorAfter

			if self.verbose: print(' improvement:', format(100*( (errorBefore-errorAfter)/1. ), '.2f') + "%\n")


		if displayTest: print('Average error on training data =', errorAfterCombined/float(nnData.sizeOfTrainingData()))
		if displayTest: print('Error on training example with highest error =', highestError, '\n\n')


		if separateTestData:

			goalError = 0.29
			testsPassed = 0
			for inputValues, desiredOutputValues in nnData.getAllTestData():

				nnOutput = nn.propagate(inputValues)#use new weights to get output values in order to do error calculation
				errorRate = nn.errorRate(nn.getOutputs(), desiredOutputValues)
				if errorRate < goalError:
					testsPassed += 1

				if displayTest:
					print('Test: err =', format(errorRate, '.4f'), '\tdesired out:', desiredOutputValues)
					print('in:', inputValues, '\tout:' , nnOutput, '\n')

			#if TSEGAchieved and
			#if neural network gives root mean square error for 80% of tests from test data smaller than goalError
			if TSSEGAchieved and ( (testsPassed / nnData.sizeOfTestData()) > 0.8 ):
				print('Achieved error of less than', goalError, 'on 80% of test cases and less than', TSSEG, 'on all training data.')
				if nnData.sizeOfTestData() <= 10: self.verbose = True
				return True

		else:
			return False