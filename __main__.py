from NeuralNetworkManager import NeuralNetworkManager

from NNTests import LogicFunctionsTest, IrisTest, MNISTTest

#to achieve better quality model decrease TSSEG and goalError in NeuralNetworkManager
def main():

	print('program start')
	#uncomment one at the time

	#lft = LogicFunctionsTest()

	it = IrisTest()#may get stuck because local minima; stop with ctrl+c

	#MNISTTest(True)

	#initManually()
	
	#trainXORandSaveToFile()
	#loadXORFromFile()

	print('program end')


def trainXORandSaveToFile():
	numberOfInputNeurons = 2
	numberOfOutputNeurons = 1
	#XOR
	inputs = [[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0]]
	outputs = [[0.0], [1.0], [1.0], [0.0]]
	#extra inputs and outputs at position 0 because those will be used as test, so nn wont see them during training

	nnm = NeuralNetworkManager([numberOfInputNeurons, 3, numberOfOutputNeurons], inputs, outputs, False, 20000, False)
	nnm.saveNN('model_2_3_1_XOR.txt')

def loadXORFromFile():
	numberOfInputNeurons = 2
	numberOfOutputNeurons = 1
	#XOR
	inputs = [[0.0, 0.0], [1.0, 0.0], [1.0, 0.0], [1.0, 1.0]]
	outputs = [[0.0], [1.0], [1.0], [0.0]]
	#extra inputs and outputs at position 0 because those will be used as test, so nn wont see them during training

	#run for 0 epochs in order to initizalize NeuralNetworkManager
	nnm = NeuralNetworkManager([numberOfInputNeurons, 3, numberOfOutputNeurons], inputs, outputs, False, 0, False)
	
	nnm.loadNN(inputs, outputs, 'model_2_3_1_XOR.txt')

	while True:
		res = nnm.guess([ float(input('Enter param 1:')), float(input('Enter param 2:')) ], False)
		print('nn guess:', res, '\n')


def initManually():

	from NeuralNetwork import NeuralNetwork
	from Neuron import Neuron

	n1 = Neuron(0, None)
	n2 = Neuron(1, None)
	n3 = Neuron(0, [0.6, 0.7])
	n4 = Neuron(0, [0.9])
	n5 = Neuron(1, [0.8])

	nn = NeuralNetwork(3)
	
	nn.addNeuronsToLayer(1, [n1, n2])#input layer
	nn.addNeuronsToLayer(2, [n3])#hidden layer
	nn.addNeuronsToLayer(3, [n4, n5])#output layer
	
	
	print(nn)
	nn.showGraph()
	print()
	
	
	inputValues = [1., 0.]
	print('input values:', inputValues)
	
	nnOutput = nn.propagate(inputValues)#must run propagate before backprop in order to set outputs for derivative calculations
	print('output: ' + nnOutput)

	targetValues = [0., 1.]
	print('targetValues:', targetValues, '\n')
	error = nn.errorRate(nn.getOutputs(), targetValues)
	print('Error:', error, '\n')

	i = 0
	while i < 500:
		i += 1

		print('Epoch', i)

		print('running backprop')
		nn.backprop(targetValues)
		
		nnOutput = nn.propagate(inputValues)
		print('output: ' + nnOutput)

		error = nn.errorRate(nn.getOutputs(), targetValues)
		print('Error:', error)
		print()


if __name__ == '__main__':
	main()