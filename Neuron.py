import math

class Neuron():
	
	#Neuron should have inputWeight for every Neuron in previus layer as self.weights weigh the signal coming from prevoius layer neurons
	def __init__(self, id, inputWeights):
		self.weights = inputWeights#each neuron has weights list which contains all weights from it to neurons of previous layer
		self.output = .1#must not be 0
		self.ETA = 0.15
		self.learningRate = 0.08#this is extremely important and relevent to the type of data and number of neurons used
		
		self.id = id#used so neurons in next layer can identify within its weights which one links to this neuron

		self.myGradient = 0.5#overwritten before used
		self.bias = 0.0#this should be changed when activationFunc is changed

	#inputValues is a list conataining values that are output of all neurons on previous layer
	def signal(self, inputValues):
		
		if isinstance(self.weights, type(None)):#first layer signal
			self.output = inputValues[self.id]
			return self.output
		
		#if not first layer
		y = 0
		
		for x, w in zip(inputValues, self.weights):
			y += x * w

		self.output = self.activationFunc(y+self.bias)
		return self.output
		

	def activationFunc(self, x):
		#relu
		#return max(0,x)

		#sigmoid
		#return 1/(1+math.exp(-x))

		#tanh
		return math.tanh(x)
		#return (max(0., math.tanh(x)))#clamp so that rmsq is correct

	def activationFuncDerivative(self, x):
		#relu
		#if x > 0: return x
		#else: return 0.01

		#sigmoid
		#return self.activationFunc(x) * (1 - self.activationFunc(x))

		#tanh
		return 1 - math.pow(math.tanh(x), 2.)


	def updateWeights(self, nextLayerNeurons, previousLayerNeurons):
		
		myGradient = self.activationFuncDerivative(self.output)

		effectOfNextLayer = 0
		for neuron in nextLayerNeurons:

			weightOfNeuronConnectedToMe   = neuron.weights[self.id]
			gradientOfNeuronConnectedToMe = neuron.myGradient
			effectOfNextLayer += weightOfNeuronConnectedToMe * gradientOfNeuronConnectedToMe

		self.myGradient = effectOfNextLayer * myGradient


		#update weights connecting this neuron to neurons in previous layer
		for neuron in previousLayerNeurons:
			self.weights[neuron.id] += self.ETA * neuron.output * self.myGradient


	def updateOutputLayerGradientAndWeights(self, targetValues, previousLayerNeurons):
		
		myGradient = self.activationFuncDerivative(self.output)

		targetValueForThisNeuron = targetValues[self.id]
		self.myGradient = self.learningRate*(targetValueForThisNeuron - self.output) * myGradient


		#update weights connecting this neuron to neurons in previous layer
		for neuron in previousLayerNeurons:
			self.weights[neuron.id] += self.ETA * neuron.output * self.myGradient


	#weights connect this neuron to previous layer neurons
	def __str__(self):
		try:
			res = ''
			for weight in self.weights:
				res += format(weight, '.3f') + '; '
			return 'weights:' + str(res)
		except TypeError as e:#'NoneType' object is not iterable
			#this happens when self.weights = None which is case for input layer
			return 'weights:' + str(self.weights)