from os import path
#example line
#0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,116,125,171,255,255,150,93,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,169,253,253,253,253,253,253,218,30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,169,253,253,253,213,142,176,253,253,122,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,52,250,253,210,32,12,0,6,206,253,140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,77,251,210,25,0,0,0,122,248,253,65,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,31,18,0,0,0,0,209,253,253,65,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,117,247,253,198,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,76,247,253,231,63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,253,253,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,176,246,253,159,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,25,234,253,233,35,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,198,253,253,141,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,78,248,253,189,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,19,200,253,253,141,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,134,253,253,173,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,248,253,253,25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,248,253,253,43,20,20,20,20,5,0,5,20,20,37,150,150,150,147,10,0,0,0,0,0,0,0,0,0,248,253,253,253,253,253,253,253,168,143,166,253,253,253,253,253,253,253,123,0,0,0,0,0,0,0,0,0,174,253,253,253,253,253,253,253,253,253,253,253,249,247,247,169,117,117,57,0,0,0,0,0,0,0,0,0,0,118,123,123,123,166,253,253,253,155,123,123,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,num2

class ImageProcessing():

	#csv file with 1D represented black and white images and 'numX' in last column
	#self.fileName = ex.'D:\data\mnist_test.csv'

	def __init__(self, fileName):
		fullPath = path.join(path.dirname(__file__), 'data', fileName)
		self.fileName = fullPath


	#return tuple with 1D image and value it represents, ex.: ([1,5,0,0,255,242,....], 7)
	def getLineAsList(self, lineNumber):
		numberOfLinesToSkip = lineNumber-1
		s = ''
		lineNum = -1
		with open(self.fileName, 'r') as f:
			for _ in range(numberOfLinesToSkip):
				next(f)
			line = f.readline()
			s = eval('[' + line[:-6] + ']')#turn line into a list (and remove ',numX\n' from the end)
			lineNum = int(line[-2:-1])#get X from 'numX\n' at the end of the line
		return (s, lineNum)


	def processLineAndDisplayIt(self, lineNumber):
		numberOfLinesToSkip = lineNumber-1
		lineNum = -1
		with open(self.fileName, 'r') as f:
			for _ in range(numberOfLinesToSkip):
				next(f)
			line = f.readline()
			s = eval('[' + line[:-6] + ']')#turn line into a list (and remove ',numX\n' from the end)
			self.display(s, 28)
			lineNum = line[-5:]#get numX from 'numX\n' at the end of the line
			print(lineNum)
		return lineNum

	#s is a 1d list representation of 2d array (black and white image) whose vertical and horizontal sizes are the same
	def display(self, s, size):

		for i in range(size):
			for j in range(size):
				if s[i*size+j] > 10:#pixel value more than 0 (there could be more elif-s to show gray parts)
					print(chr(0x25A1), end='')
				else:
					print(chr(0x25A0), end='')
			print()