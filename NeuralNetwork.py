from Neuron import Neuron
from math import sqrt

class NeuralNetwork():
	
	#layersNum - number of layers the nn has
	#self.nn - list size is 0 to layersNum-1; self.nn[i] will give list of neurons for layer i

	def __init__(self, layersNum):
		self.layersNum = layersNum
		self.nn = [[] for i in range(self.layersNum)]

		self.layerOutputPrintPrecision = 3
		
	def addNeuronsToLayer(self, layerNumber, listOfNeurons):

		for neuron in listOfNeurons:
			
			self.nn[layerNumber-1].append(neuron)
			

	def save(self, fileName):

		with open(fileName, 'w') as fp:
		
			for layer in self.nn[1:]:#every layer except input layer
				
				for neuron in layer:

					fp.write(str(neuron.weights)+'\n')

		self.showGraph()
		print()
		print(self.__str__())
		print('Saved model to', fileName)

	def load(self, fileName):

		with open(fileName, 'r') as fp:
		
			for layer in self.nn[1:]:#every layer except input layer
				
				for neuron in layer:

					neuron.weights = eval(fp.readline())
					assert type(neuron.weights) == list, 'Error: non list format found when loading model'

		self.showGraph()
		print()
		print(self.__str__())
		print('Loaded model from', fileName)


	#previousLayerOutput is a list with outputs of every neuron in previous layer
	def propagate(self, previousLayerOutput):
		
		output = []
		
		#go through all layers
		for layer in range(self.layersNum):
			
			for neuron in self.nn[layer]:
				output.append( neuron.signal(previousLayerOutput) )
				
			previousLayerOutput = output
			#print('layer', layer+1, 'output:', list(float(format(i, '.'+str(self.layerOutputPrintPrecision)+'f')) for i in output))
			
			output = []
			

		return ', '.join(str(o) for o in list(round(i, self.layerOutputPrintPrecision) for i in previousLayerOutput))


	def backprop(self, targetValues):
		
		#all layers (except for input layer) starting from output layer
		for layer in reversed(range(1, self.layersNum)):

			if layer == self.layersNum-1:#output layer

				#calculate output layer gradient
				for neuron in self.nn[layer]:

					previousLayerNeurons = self.nn[layer-1]

					neuron.updateOutputLayerGradientAndWeights(targetValues, previousLayerNeurons)

				
			else:#hidden layer
				for neuron in self.nn[layer]:

					nextLayerNeurons = self.nn[layer+1]
					previousLayerNeurons = self.nn[layer-1]

					neuron.updateWeights(nextLayerNeurons, previousLayerNeurons)
				
	
	#returns outputs of nn in list format
	def getOutputs(self):
		outputs = []
		for neuron in self.nn[self.layersNum-1]:
			outputs.append(neuron.output)
		return outputs
	
	#Root Mean Square Error
	def errorRate(self, actualOutput, desiredOutput):

		s = 0

		for output, target in zip(actualOutput, desiredOutput):
			s += (target - output)**2#max is needed because tanh can produce negative output

		numOfOutputNeurons = len(actualOutput)

		return sqrt(s/numOfOutputNeurons)

		#return 0.5*((desiredOutput - actualOutput)**2/len(desiredOutput))
	
		
	def __str__(self):

		sNN = ''
		for i, layer in enumerate(self.nn):
			assert len(layer) != 0, 'Error: empty layer:' + str(i+1)
			sNN += 'Layer ' + str(i+1) + ': '
			sNN += ' '.join([str(neuron) for neuron in layer])
			sNN += '\n'
		
		return sNN

	def showGraph(self):
		for i, layer in enumerate(self.nn):
			o = '*'*len(layer)
			s = ''.join([' ' for j in range(30-len(layer))])
			o += s
			print('Layer', str(i+1)+':', o, '\t', ' '.join(['ID:'+str(neuron.id) for neuron in layer]))
