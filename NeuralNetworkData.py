from random import randrange

class NeuralNetworkData():


	#ex. for AND function:
	#inputValues = [[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0]]
	#targetValues = [[0.0], [0.0], [0.0], [1.0]]
	def __init__(self, inputValues, targetValues, separateTestData):


		if len(inputValues) != len(targetValues):
			raise SystemExit('length of inputValues not equal to length of targetValues')


		self.testDataInput = []
		self.testDataOutput = []

		self.trainingDataInput = []
		self.trainingDataOutput = []

		if separateTestData == False:
			self.trainingDataInput = inputValues
			self.trainingDataOutput = targetValues
			return


		if len(targetValues) <= 10:

			print('Taking first example as test and using rest for training')

			self.testDataInput.append(inputValues[0])
			self.testDataOutput.append(targetValues[0])

			for i, o in zip(inputValues[1:], targetValues[1:]):
				self.trainingDataInput.append(i)
				self.trainingDataOutput.append(o)


		elif len(targetValues) <= 25:

			print('Taking randomly around 10% as test and using rest for training')

			randTest = []

			testSize = int(len(targetValues)/10)
			for test in range(testSize):
				randTest.append( randrange(0, len(targetValues)) )


			for itt, (i, o) in enumerate(zip(inputValues, targetValues)):

				if itt in randTest:#takes randomly around 10% of targetValues to be used for test
					self.testDataInput.append(i)
					self.testDataOutput.append(o)
				else:
					self.trainingDataInput.append(i)
					self.trainingDataOutput.append(o)


		elif len(targetValues) <= 100:

			print('Taking randomly around 5% as test and using rest for training')

			randTest = []

			testSize = int(len(targetValues)/25)
			for test in range(testSize):
				randTest.append( randrange(0, len(targetValues)) )


			for itt, (i, o) in enumerate(zip(inputValues, targetValues)):

				if itt in randTest:#takes randomly around 5% of targetValues to be used for test
					self.testDataInput.append(i)
					self.testDataOutput.append(o)
				else:
					self.trainingDataInput.append(i)
					self.trainingDataOutput.append(o)


		elif len(targetValues) <= 1000:

			print('Taking randomly around 2% as test and using rest for training')

			randTest = []

			testSize = int(len(targetValues)/50)
			for test in range(testSize):
				randTest.append( randrange(0, len(targetValues)) )


			for itt, (i, o) in enumerate(zip(inputValues, targetValues)):

				if itt in randTest:#takes randomly around 2% of targetValues to be used for test
					self.testDataInput.append(i)
					self.testDataOutput.append(o)
				else:
					self.trainingDataInput.append(i)
					self.trainingDataOutput.append(o)


		elif len(targetValues) <= 10000:

			print('Taking randomly around 0.2% as test and using rest for training')

			randTest = []

			testSize = int(len(targetValues)/500)#set to 4000 to only see 2 test cases
			for test in range(testSize):
				randTest.append( randrange(0, len(targetValues)) )


			for itt, (i, o) in enumerate(zip(inputValues, targetValues)):

				if itt in randTest:#takes randomly around 0.2% of targetValues to be used for test
					self.testDataInput.append(i)
					self.testDataOutput.append(o)
				else:
					self.trainingDataInput.append(i)
					self.trainingDataOutput.append(o)


		else:
			raise SystemExit('len(targetValues) > 10000 for test values in NeuralNetworkData NOT IMPLEMENTED')
			
			
		#make sure that because separateTestData is True there is at least one test data sample created
		assert len(self.testDataOutput) > 0, 'bug in NeuralNetworkData, testDataOutput size is 0 or less'

		assert len(self.testDataInput)==len(self.testDataOutput), 'bug in NeuralNetworkData, testDataInput and testDataOutput sizes dont match'


	def sizeOfTestData(self): return len(self.testDataOutput)
	def sizeOfTrainingData(self): return len(self.trainingDataOutput)

	def getAllTestData(self): return zip(self.testDataInput, self.testDataOutput)
	def getAllTrainingData(self): return zip(self.trainingDataInput, self.trainingDataOutput)