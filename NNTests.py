from NeuralNetworkManager import NeuralNetworkManager
from DataSet import DataSet
from ImageProcessing import ImageProcessing

class LogicFunctionsTest():

	def __init__(self):
		print('running LogicFunctionsTest')

		roundOutputsIntoInts = True
		
		numberOfInputNeurons = 2
		numberOfOutputNeurons = 1

		#XOR
		inputs = [[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0]]
		outputs = [[0.0], [1.0], [1.0], [0.0]]

		nnm = NeuralNetworkManager([numberOfInputNeurons, 3, numberOfOutputNeurons], inputs, outputs, False, 20000)
		for i, o  in zip(inputs, outputs):
			if nnm.guess(i, roundOutputsIntoInts) != o:
				print('for i:', i, 'got o', nnm.guess(i, roundOutputsIntoInts), 'expected', o)
				raise SystemExit('LogicFunctionsTest XOR test failed')

		#OR
		inputs = [[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0]]
		outputs = [[0.0], [1.0], [1.0], [1.0]]

		nnm = NeuralNetworkManager([numberOfInputNeurons, 3, numberOfOutputNeurons], inputs, outputs, False, 20000)
		for i, o  in zip(inputs, outputs):
			if nnm.guess(i, roundOutputsIntoInts) != o:
				print('for i:', i, 'got o', nnm.guess(i, roundOutputsIntoInts), 'expected', o)
				raise SystemExit('LogicFunctionsTest OR test failed')

		#AND
		inputs = [[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0]]
		outputs = [[0.0], [0.0], [0.0], [1.0]]

		nnm = NeuralNetworkManager([numberOfInputNeurons, 3, numberOfOutputNeurons], inputs, outputs, False, 20000)
		for i, o  in zip(inputs, outputs):
			if nnm.guess(i, roundOutputsIntoInts) != o:
				print('for i:', i, 'got o', nnm.guess(i, roundOutputsIntoInts), 'expected', o)
				raise SystemExit('LogicFunctionsTest AND test failed')

		print('LogicFunctionsTest finished successfully')


#Note: takes ~30sec (if it fails press ctrl+c and rerun)
class IrisTest():

	def __init__(self):
		print('running IrisTest')

		dataSetFileName = 'iris.data'#csv file
		numberOfColumns = 5
		indexOfLastInput = 4

		dataSet = DataSet(dataSetFileName, numberOfColumns, indexOfLastInput)

		#getNumberOfOutputs can be larger number then numberOfColumns in csv
		dataSetOutputNeurons = dataSet.getNumberOfOutputs()
		numberOfInputNeurons = indexOfLastInput

		#nnm = NeuralNetworkManager([numberOfInputNeurons, 10, dataSetOutputNeurons], dataSet.inputs, dataSet.outputs, True, 1000000)
		nnm = NeuralNetworkManager([numberOfInputNeurons, 8, 8, dataSetOutputNeurons], dataSet.inputs, dataSet.outputs, True, 100000)
		
		dataSet.printMapping()
		print('IrisTest finished successfully')

		try:

			while True:
				res = nnm.guess([float(input('Enter param 1:')),float(input('Enter param 2:')),float(input('Enter param 3:')),float(input('Enter param 4:'))], True)
				print(res, end=' ie. ')
				for k, v in dataSet.getMapping().items():#k is a sting, v is a list(ex. [1,0,0])
					if res == v: print(k)

		except KeyboardInterrupt: print('Ctrl+C')


#Note: takes ~1 min for every epoch with 2 hidden layers of 8 neurons each (because input layer has 784 neurons)
class MNISTTest():

	def __init__(self, loadFromFile = True):
		print('running MNISTTest')

		dataSetFileName = 'mnist_test.csv'#csv file
		numberOfColumns = 785
		indexOfLastInput = 784

		dataSet = DataSet(dataSetFileName, numberOfColumns, indexOfLastInput)

		#getNumberOfOutputs can be larger number then numberOfColumns in csv
		dataSetOutputNeurons = dataSet.getNumberOfOutputs()
		numberOfInputNeurons = indexOfLastInput


		nnm = None
		if loadFromFile:
			nnm = NeuralNetworkManager([numberOfInputNeurons, 8, 8, dataSetOutputNeurons], dataSet.inputs, dataSet.outputs, False, 0)
			nnm.loadNN(dataSet.inputs, dataSet.outputs, 'model_784_8_8_10_MNIST_20epochs.txt')
		else:
			nnm = NeuralNetworkManager([numberOfInputNeurons, 8, 8, dataSetOutputNeurons], dataSet.inputs, dataSet.outputs, True, 10)
		
		dataSet.printMapping()
		print('MNISTTest finished successfully')

		try:

			lineNumber = 0
			while True:

				ip = ImageProcessing(dataSetFileName)
				inputValues, correctValue = ip.getLineAsList(lineNumber)

				print('\nActual image:')
				ip.processLineAndDisplayIt(lineNumber)

				print('guess: ', end='')
				res = nnm.guess(inputValues, True)
				print(res, end=' ie. ')
				for k, v in dataSet.getMapping().items():#k is a string, v is a list(ex. [1,0,0])
					if res == v: print(k)

				lineNumber = int(input('\n\nEnter line number:'))

		except KeyboardInterrupt: print('Ctrl+C')

		#nnm.saveNN('model_784_8_8_10_MNIST_XXepochs.txt')#enable this to overwrite file with new weights